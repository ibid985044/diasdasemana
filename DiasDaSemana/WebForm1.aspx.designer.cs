﻿//------------------------------------------------------------------------------
// <gerado automaticamente>
//     Esse código foi gerado por uma ferramenta.
//
//     As alterações ao arquivo poderão causar comportamento incorreto e serão perdidas se
//     o código for recriado
// </gerado automaticamente>
//------------------------------------------------------------------------------

namespace DiasDaSemana
{


    public partial class WebForm1
    {

        /// <summary>
        /// Controle tipsForm.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm tipsForm;

        /// <summary>
        /// Controle chkDaysOfWeek.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.CheckBoxList chkDaysOfWeek;

        /// <summary>
        /// Controle btnSubmit.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnSubmit;

        /// <summary>
        /// Controle lblSelectedDays.
        /// </summary>
        /// <remarks>
        /// Campo gerado automaticamente.
        /// Para modificar, mova a declaração de campo do arquivo de designer a um arquivo code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblSelectedDays;
    }
}
