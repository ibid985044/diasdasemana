﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DiasDaSemana
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            // Criar uma string para armazenar os dias selecionados
            string selectedDays = "";

            // Percorrer os itens do CheckBoxList e verificar se eles estão marcados
            foreach (ListItem item in chkDaysOfWeek.Items)
            {
                if (item.Selected)
                {
                    selectedDays += item.Text + ", ";
                }
            }

            // Remover a vírgula final e atualize o texto da etiqueta
            selectedDays = selectedDays.TrimEnd(' ', ',');
            lblSelectedDays.Text = "Dias selecionados: " + selectedDays;

        }
    }
}