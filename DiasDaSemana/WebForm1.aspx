﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="DiasDaSemana.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-br">
<head runat="server">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link href="Style.css" rel="stylesheet" type="text/css" />
    <title></title>
</head>
<body>
    <div id="container">
        <h1>Quais dias da semana estará disponível para trabalho?</h1>
        <form id="tipsForm" runat="server">
                <asp:CheckBoxList ID="chkDaysOfWeek" runat="server">
                <asp:ListItem Text="Domingo" Value="Sunday" />
                <asp:ListItem Text="Segunda-feira" Value="Monday" />
                <asp:ListItem Text="Terça-feira" Value="Tuesday" />
                <asp:ListItem Text="Quarta-feira" Value="Wednesday" />
                <asp:ListItem Text="Quinta-feira" Value="Thursday" />
                <asp:ListItem Text="Sexta-feira" Value="Friday" />
                <asp:ListItem Text="Sábado" Value="Saturday" />
                </asp:CheckBoxList>
                <asp:Button ID="btnSubmit" runat="server" Text="Enviar" OnClick="btnSubmit_Click" /><br /><br />
                <asp:Label ID="lblSelectedDays" runat="server" />
        </form>
    </div>
</body>
</html>
